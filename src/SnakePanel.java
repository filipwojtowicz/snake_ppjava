import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SnakePanel extends JPanel implements KeyListener {

    private enum Direction {
        N, W, E, S;
    }

    public int getNumberOfRows() {
        return NumberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        NumberOfRows = numberOfRows;
    }

    public int getNumberOfColumns() {
        return NumberOfColumns;
    }

    public void setNumberOfColumns(int numberOfColumns) {
        NumberOfColumns = numberOfColumns;
    }

    public int _width = 400;
    public int _height = 400;

    public ArrayList<Point> snake;
    public ArrayList<Point> notDigestedFood;

    public int moveFreq = 20; // move/250ms
    public int renderFreq = 20;

    private int NumberOfRows;
    private int NumberOfColumns;

    private int score;
    public boolean gameOver;
    
    public Point food;

    private Direction direction;
    private Direction nextDirection;

    public Timer render;
    public Timer animation;
    private int times;  //zlicza przesuniecia weza, by byl dobrze ustawiony

    public int scale = 20; //musi byc dzielnikiem szerokosci i wysokosci

    public SnakePanel() {
        init();
    }

    private boolean isSnake(Point there) {
        for (Point point : snake)
            if (point.equals(there))
                return true;

        return false;
    }

    private void start() {
        animation = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (times % scale == 0) {
                    direction = nextDirection;
                    times = 0;
                }
                Point next = new Point(snake.get(0));
                if (direction == Direction.E) next.setLocation((snake.get(0).x + 1) % _width, snake.get(0).y);
                if (direction == Direction.W) next.setLocation((snake.get(0).x + _width - 1) % _width, snake.get(0).y);
                if (direction == Direction.S) next.setLocation(snake.get(0).x, (snake.get(0).y + 1) % _height);
                if (direction == Direction.N) next.setLocation(snake.get(0).x, (snake.get(0).y + _height - 1) % _height);;

                if (next.equals(food)) {
                    notDigestedFood.add(new Point(food));
                    score += 100;
                    generateFood();
                }

                snake.get(snake.size() - 1);

                if (isSnake(next)) {
                    gameOver = true;
                    animation.cancel();
                    return;
                }
                if (!notDigestedFood.isEmpty()) {
                    if (snake.get(snake.size() - 1).equals(notDigestedFood.get(0))) {
                        snake.add(new Point(notDigestedFood.get(0)));
                        notDigestedFood.remove(0);
                    }
                }
                if (times % scale == 0) {
                    for (int i = snake.size() - 1; i > 0; i--) {
                        snake.set(i, new Point(snake.get(i - 1)));
                    }
                }
                snake.set(0, new Point(next));

                times++;
            }
        };
        animation.scheduleAtFixedRate(task, 0, moveFreq);
    }
    private void generateFood() {
        Random generate = new Random();
        food = new Point(0, 0);
        int i = generate.nextInt(NumberOfColumns) * scale, j = generate.nextInt(NumberOfRows) * scale;
        food.setLocation(i ,j);
        while (isSnake(food)) {
            i = generate.nextInt(NumberOfColumns) * scale;
            j = generate.nextInt(NumberOfRows) * scale;
            food.setLocation(i, j);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT && direction!=Direction.E) nextDirection = Direction.W;
        if (key == KeyEvent.VK_RIGHT && direction!=Direction.W) nextDirection = Direction.E;
        if (key == KeyEvent.VK_UP && direction!=Direction.S) nextDirection = Direction.N;
        if (key == KeyEvent.VK_DOWN && direction!=Direction.N) nextDirection = Direction.S;
    }

    public void init() {
        NumberOfRows = _height/scale;
        NumberOfColumns = _width/scale;
        score = 0;
        gameOver = false;
        snake = new ArrayList<>();
        notDigestedFood = new ArrayList<>();
        times = 0;

        if (animation != null)
            animation.cancel();
        if (render != null)
            render.cancel();

        generateFood();
        try {
            snake.add(new Point(0,0));
        } catch (Exception e) {
            System.out.println(e);
        }
        nextDirection = Direction.E;
        addKeyListener(this);
        render = new Timer();
        TimerTask taskRender = new TimerTask() {
            @Override
            public void run() {
                repaint();
            }
        };
        render.scheduleAtFixedRate(taskRender, 0, renderFreq);
        start();
    }

    public void init(int renderFreqN, int moveFreqN, int scaleN, int _heightN, int _widthN) {
        renderFreq = renderFreqN;
        moveFreq = moveFreqN;
        scale = scaleN;
        _height = _heightN;
        _width = _widthN;
        NumberOfRows = _height/scale;
        NumberOfColumns = _width/scale;
        score = 0;
        gameOver = false;
        snake = new ArrayList<>();
        notDigestedFood = new ArrayList<>();
        times = 0;

        if (animation != null)
            animation.cancel();
        if (render != null)
            render.cancel();

        generateFood();
        try {
            snake.add(new Point(0,0));
        } catch (Exception e) {
            System.out.println(e);
        }
        nextDirection = Direction.E;
        addKeyListener(this);
        render = new Timer();
        TimerTask taskRender = new TimerTask() {
            @Override
            public void run() {
                repaint();
            }
        };
        render.scheduleAtFixedRate(taskRender, 0, renderFreq);
        start();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, _width + scale, _height + scale);

        g.setColor(Color.GREEN);
        for (Point point : snake) {
            g.fillRect(point.x, point.y, scale, scale);
        }

        g.setColor(Color.YELLOW);
        for (Point point : notDigestedFood)
            g.fillRect(point.x, point.y, scale, scale);

        g.setColor(Color.RED);
        g.fillRect(food.x, food.y, scale, scale);

        String string = "Score: " + score;

        g.setColor(Color.white);

        g.drawString(string, (_width / 2 - string.length()), 10);

        string = "Game Over!";

        if (gameOver)
            g.drawString(string, (_width / 2 - string.length()), (_height / 2));
    }
}
