import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.*;
/*
 * Created by JFormDesigner on Sun May 13 16:59:26 CEST 2018
 */



/**
 * @author Filip Wojtowicz
 */
public class MainWindow extends JFrame implements ActionListener {

    private JPanel contentPanel;
    private JButton restartButton;
    private SnakePanel snakePanel;
    private TextField renderFreq;
    private TextField moveFreq;
    private TextField scale;
    private TextField heigth;
    private TextField width;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MainWindow() {
        initComponents();
    }

    private void initComponents() {

        setBounds(100, 100, 1200, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        contentPanel = new JPanel();
        contentPanel.setBorder(new EmptyBorder(5,5,5,5));
        setContentPane(contentPanel);
        contentPanel.setLayout(new MigLayout("", "[grow][]","[grow][]"));

        snakePanel = new SnakePanel();
        snakePanel.setFocusable(true);
        snakePanel.requestFocus();
        contentPanel.add(snakePanel, "grow, wrap");
        restartButton = new JButton("Restart");
        restartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                snakePanel.init(
                        Integer.parseInt(renderFreq.getText()),
                        Integer.parseInt(moveFreq.getText()),
                        Integer.parseInt(scale.getText()),
                        Integer.parseInt(heigth.getText()),
                        Integer.parseInt(width.getText()));
                snakePanel.setFocusable(true);
                snakePanel.requestFocus();
            }
        });
        contentPanel.add(new JLabel("Czestotliwosc odswiezania (ms)"));
        renderFreq = new TextField();
        renderFreq.setText("20");
        contentPanel.add(renderFreq, "split 4");
        contentPanel.add(new JLabel("Czestotliwosc ruchow (ms)"));
        moveFreq = new TextField();
        moveFreq.setText("20");
        contentPanel.add(moveFreq, "wrap");
        contentPanel.add(new JLabel("Rozmiar komponentu (powinien byc dzielnikiem wymiarow)"), "split 2");
        scale = new TextField();
        scale.setText("20");
        contentPanel.add(scale, "wrap");

        contentPanel.add(new JLabel("Wysokosc planszy"), "split 4");
        heigth = new TextField();
        heigth.setText("400");
        contentPanel.add(heigth);
        contentPanel.add(new JLabel("Szerokosc planszy"));
        width = new TextField();
        width.setText("400");
        contentPanel.add(width, "wrap");

        contentPanel.add(restartButton);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
